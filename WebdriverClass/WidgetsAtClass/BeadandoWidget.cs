﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class BeadandoWidget : BasePage
    {
        public BeadandoWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        private List<IWebElement> firstPageRadioElementsLists;
        private ReadOnlyCollection<IWebElement> firstPageRadioElements;

        private void SetListForRadioElements()
        {
            firstPageRadioElementsLists = new List<IWebElement>();
            firstPageRadioElementsLists.Add(Driver.FindElement(By.CssSelector(
                "#question-field-373484282 > fieldset > div > div > div:nth-child(1) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated")));
            firstPageRadioElementsLists.Add(Driver.FindElement(By.CssSelector(
                "#question-field-373484282 > fieldset > div > div > div:nth-child(2) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated")));
            firstPageRadioElementsLists.Add(Driver.FindElement(By.CssSelector(
                "#question-field-373484282 > fieldset > div > div > div:nth-child(3) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated")));
            firstPageRadioElementsLists.Add(Driver.FindElement(By.CssSelector(
                "#question-field-373484282 > fieldset > div > div > div:nth-child(4) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated")));
            firstPageRadioElements = firstPageRadioElementsLists.AsReadOnly();
        }

        private IWebElement RadioButtonForSecondTask =>
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated"));

        private IWebElement ComboBoxForThirdTask =>
            Driver.FindElement(By.Id("373480133"));

        private List<IWebElement> listForFourthTask;
        private ReadOnlyCollection<IWebElement> containerForFourthTask;

        private void SetListForFourthTask()
        {
            listForFourthTask=new List<IWebElement>();
            listForFourthTask.Add(Driver.FindElement(By.Id("373480134_2479050988")));
            listForFourthTask.Add(Driver.FindElement(By.Id("373480134_2479050997")));
            listForFourthTask.Add(Driver.FindElement(By.Id("373480134_2479050998")));
            listForFourthTask.Add(Driver.FindElement(By.Id("373480134_2479050999")));
            listForFourthTask.Add(Driver.FindElement(By.Id("373480134_2479051000")));
            containerForFourthTask = listForFourthTask.AsReadOnly();
        }

        private IWebElement PressNextButtonOnFirstPage => Driver.FindElement(By.CssSelector(".btn"));

        private IWebElement RadioButtonForFifthTask =>
            Driver.FindElement(By.CssSelector(
                "#question-field-373480135 > fieldset > div > div > div:nth-child(2) > div > label > span.radio-button-label-text.question-body-font-theme.user-generated"));

        private IWebElement MessageBoxForSixthTask => 
            Driver.FindElement(By.Id("373480136"));

        private IWebElement DoneButton =>
            Driver.FindElement(By.CssSelector("button.btn:nth-child(2)"));
        public void FirstTask(int idx)
        {
            SetListForRadioElements();
            firstPageRadioElements[idx].Click();
        }

        public void SecondTask()
        {
            RadioButtonForSecondTask.Click();
        }

        public void ThirdTask(string text)
        {
            ComboBoxForThirdTask.Click();
            new SelectElement(ComboBoxForThirdTask).SelectByText(text);
        }

        public void FourthTask()
        {
            SetListForFourthTask();
            containerForFourthTask[0].Click();
            new SelectElement(containerForFourthTask[0]).SelectByIndex(2);
            containerForFourthTask[1].Click();
            new SelectElement(containerForFourthTask[1]).SelectByIndex(1);
            containerForFourthTask[2].Click();
            new SelectElement(containerForFourthTask[2]).SelectByIndex(3);
            containerForFourthTask[3].Click();
            new SelectElement(containerForFourthTask[3]).SelectByIndex(4);
            containerForFourthTask[4].Click();
            new SelectElement(containerForFourthTask[4]).SelectByIndex(5);
        }

        public void NextToSecondPage()
        {
            PressNextButtonOnFirstPage.Click();
        }

        public void FifthTask()
        {
            RadioButtonForFifthTask.Click();
        }

        public void SixthTask(string message)
        { 
            MessageBoxForSixthTask.SendKeys(message);
        }
        
        public void SendSurvey()
        {
            DoneButton.Click();
        }
    }
}