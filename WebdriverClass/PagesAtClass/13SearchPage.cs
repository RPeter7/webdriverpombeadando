﻿using OpenQA.Selenium;
using WebdriverClass.WidgetsAtClass;
using SearchWidget = WebdriverClass.WidgetsAtClass.SearchWidget;

namespace WebdriverClass.PagesAtClass
{
    class SearchPage : BasePage
    {
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        { }

        // TASK 1.1: implement a static navigate function to search page which returns a search page instance
        public static SearchPage Navigate(IWebDriver webDriver, string url)
        {
            // Navigate to "http://elvira.mav-start.hu/elvira.dll/x/index?language=1"
            // Return new SearchPage instance
            webDriver.Navigate().GoToUrl(url);
            return new SearchPage(webDriver);
        }

        // TASK 1.2: implement getSearchWidget function
        public SearchWidget GetSearchWidget()
        {
            // Create new searchWidget
            return new SearchWidget(Driver);
        }

        // TASK 3.2: implement getResultWidget function and instantiate resultWidget
        public ResultWidget GetResultWidget()
        {
            // Create new resultWidget
            return new ResultWidget(Driver);
        }

        public BeadandoWidget GetBeadandoWidget()
        {
            return new BeadandoWidget(Driver);
        }
    }
}
