﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.Tests
{
    public class BeadandoPageObjectTest : TestBase
    {
        
        static IEnumerable DataDrivenTesting() => new int[] { 1, 2 };


        [Test, TestCaseSource("DataDrivenTesting")]
        public void BeadandoTest(int idx)
        {
            var page = SearchPage.Navigate(Driver, "https://www.surveymonkey.com/r/FNRVK86");
            BeadandoWidget beadandoWidget = page.GetBeadandoWidget();
            beadandoWidget.FirstTask(1);
            beadandoWidget.SecondTask();
            beadandoWidget.ThirdTask("Webdriver advanced");
            beadandoWidget.FourthTask();
            beadandoWidget.NextToSecondPage();
            beadandoWidget.FifthTask();
            beadandoWidget.SixthTask("Nincs, minden jó volt.");
            beadandoWidget.SendSurvey();
            var title = Driver.Title;
            Assert.IsTrue(title.Contains("Thank you! Create Your Own Online Survey Now!"));
        }
    }
}
